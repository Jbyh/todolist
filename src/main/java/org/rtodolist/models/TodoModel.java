package org.rtodolist.models;
/**
 * Created by jbyh on 29.11.15.

 */
public class TodoModel {

    private int id;
    private String text;
    private String date;
    private boolean complited;

    public TodoModel(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean getComplited() {
        return complited;
    }

    public void setComplited(boolean complited) {
        this.complited = complited;
    }
}

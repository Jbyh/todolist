package org.rtodolist.controllers;

import org.rtodolist.models.TodoModel;
import org.rtodolist.service.TodoListServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;


/**
 * Created by jbyh on 29.11.15.
 */
@Controller
public class TodoListController {

    @Autowired
    private TodoListServiceI todoService;

    @RequestMapping("/list")
    public String viewTodoList(ModelMap model) {
        model.addAttribute("todolist", todoService.viewTodo());
        model.addAttribute("todoObj", new TodoModel());
        return "todolist";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addTodo(@ModelAttribute TodoModel todo, ModelMap model) {
        todoService.addTodo(todo);
        return "redirect:/list";
    }

    @RequestMapping("/complite/{uid}")
    public String compliteTodo(@PathVariable("uid") Integer uid, ModelMap model) {
        todoService.compliteTodo(uid);
        return "redirect:/list";
    }

    @RequestMapping("/remove/{uid}")
    public String removeTodo(@PathVariable("uid") Integer uid) {
        todoService.deleteTodo(uid);
        return "redirect:/list";
    }

    @RequestMapping(value = "/edit/{uid}", method = RequestMethod.GET)
    public String editTodo(@PathVariable("uid") Integer uid,ModelMap model) {
        TodoModel todo = todoService.getTodoById(uid);
        model.addAttribute("todo", todo);
        return "edit";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String editTodo(@ModelAttribute TodoModel todo, ModelMap model) {
        todoService.editTodo(todo);
        return "redirect:/list";
    }
}

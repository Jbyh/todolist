package org.rtodolist.store;

import org.rtodolist.models.TodoModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * Created by jbyh on 29.11.15.
 */
@Repository
public class TodoListStore implements TodoListDAO {

    private final HibernateTemplate template;

    @Autowired
    TodoListStore(final HibernateTemplate template) {
        this.template = template;
    }

    public void addTodo (TodoModel todo) {
            this.template.save(todo);
    }

    public Collection<TodoModel> viewTodo() {
        return (List<TodoModel>) this.template.find("from TodoModel");
    }

    public void editTodo(TodoModel todo) {
        this.template.update(todo);
    }

    public void compliteTodo(int id) {
        TodoModel todo = getTodoById(id);
            todo.setComplited(true);
            this.template.update(todo);
        }

    public void deleteTodo(int id) {
        TodoModel todo = this.getTodoById(id);
        this.template.delete(todo);
    }

    public TodoModel getTodoById(int id) {
        List<TodoModel> todo = (List<TodoModel>) this.template.find("from TodoModel where id=?",id);
        return todo.get(0);
    }

}

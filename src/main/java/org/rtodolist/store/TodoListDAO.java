package org.rtodolist.store;

import org.rtodolist.models.TodoModel;

import java.util.Collection;


/**
 * Created by jbyh on 29.11.15.
 */
public interface TodoListDAO {

    public void addTodo(TodoModel todo);

    public Collection<TodoModel> viewTodo();

    public void editTodo(TodoModel todo);

    public void compliteTodo(int id);

    public void deleteTodo(int id);

    public TodoModel getTodoById(int id);
}

package org.rtodolist.service;

import org.rtodolist.models.TodoModel;
import org.rtodolist.store.TodoListDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Created by jbyh on 30.11.15.
 */
@Service
public class TodoListService implements TodoListServiceI {



    @Autowired
    TodoListDAO todoDAO;

    @Transactional
    public void deleteTodo(int id) {
        todoDAO.deleteTodo(id);
    }

    @Transactional
    public void addTodo(TodoModel todo){
        todoDAO.addTodo(todo);
    }

    @Transactional
    public Collection<TodoModel> viewTodo() {
        return todoDAO.viewTodo();
    }

    @Transactional
    public void compliteTodo(int id){
        todoDAO.compliteTodo(id);
    }

    @Transactional
    public void editTodo(TodoModel todo) {
        todoDAO.editTodo(todo);
    }

    public TodoModel getTodoById(int id) {
        return todoDAO.getTodoById(id);
    }

    public TodoListDAO getTodoDAO() {
        return todoDAO;
    }

    public void setTodoDAO(TodoListDAO todoDAO) {
        this.todoDAO = todoDAO;
    }
}

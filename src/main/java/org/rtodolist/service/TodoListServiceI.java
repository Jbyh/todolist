package org.rtodolist.service;

import org.rtodolist.models.TodoModel;

import java.util.Collection;


/**
 * Created by jbyh on 30.11.15.
 */
public interface TodoListServiceI {
    public void deleteTodo(int id);

    public Collection<TodoModel> viewTodo();

    public void addTodo(TodoModel todo);

    public void compliteTodo(int id);

    public void editTodo(TodoModel todo);

    public TodoModel getTodoById(int id);
}

<%--
  Created by IntelliJ IDEA.
  User: jbyh
  Date: 29.11.15
  Time: 21:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
    <link href="<c:url value="/css/style.css" />" rel="stylesheet" type="text/css"/>
    <title>Todo List</title>
</head>
<body>
<style>
    .todolist-wrapper {
        width: 400px;
        height: 80px;
        border-radius: 7px;
        background-color: rgb(178, 210, 241);
        margin-bottom: 10px;
    }
    .todo-item {
        margin-right: 10px;
        float: left;
        margin-top: 35px;
        margin-left: 15px;
    }
    .todolist-remove {
        float:right;
    }
</style>
<form:form method="POST" action="/todolist/add" modelAttribute="todoObj">
    <form:label id="lable-text" path="text">Text:</form:label>
    <form:input id="text" path="text" />
    <br/>
    <form:label id="date-text" path="date">Date:</form:label>
    <form:input type="date" format="yyyy-MM-dd" path="date" />
    <form:button type="submit">add</form:button>
</form:form>
<h1>ToDo's:</h1>
<c:forEach items="${todolist}" var="todolist">
    <div class="todolist-wrapper">
        <div class="todolist-text todo-item">${todolist.text}</div>
        <div class="todolist-date todo-item">${todolist.date}</div>
        <c:choose>
            <c:when test="${todolist.complited}">
                <div class="todolitst-complite todo-item"> Complited! </div>
            </c:when>
            <c:otherwise>
                <div class="todolitst-complite todo-item"><a href="/todolist/complite/${todolist.id}">Complite</a></div>
            </c:otherwise>
        </c:choose>
        <div class="action-container todo-item">
            <div class="todolist-edit"><a href="edit/${todolist.id}">edit</a></div>
        </div>
        <div class="todolist-remove"><a href="remove/${todolist.id}">X</a></div>
    </div>
</c:forEach>

</body>
</html>

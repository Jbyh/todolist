<%--
  Created by IntelliJ IDEA.
  User: jbyh
  Date: 30.11.15
  Time: 14:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Edit ToDo</title>
</head>
<body>
    <h1>Edit ToDo</h1>
    <table id="text" class="edit-table">
        <form:form method="post" action="/todolist/edit" modelAttribute="todo" cssClass="edit-form">
            <tr>
                <td>
                    <form:label path="text" for="text">Text of Todo:</form:label>
                </td>
                <td>
                    <form:input path="text" value="${todo.text}"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="date" for="text">Date: </form:label>
                </td>
                <td>
                    <form:input path="date" value="${todo.date}" type="date" format="yyyy-MM-dd"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:input path="complited" value="${todo.complited}" type="hidden"/>
                </td>
               <td>
                    <form:input path="id" value="${todo.id}" type="hidden"/>
                </td>
            </tr>
            <tr><td><input type="submit" value="Отправить"/></td></tr>
        </form:form>
    </table>
</body>
</html>
